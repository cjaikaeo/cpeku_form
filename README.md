# CPE-KU Google Form Plugin for KidBright

ปลั๊กอินสำหรับ KidBright IDE เพื่อรองรับการส่งข้อมูลไปยัง Google Forms
ผ่านลิงก์สำหรับป้อนข้อมูลล่วงหน้า (pre-filled link) ทำให้สามารถส่งข้อมูลจากบอร์ด KidBright
ไปเก็บไว้ใน Google Sheets ได้

## การติดตั้ง วิธีที่ 1

1. ดาวน์โหลดไฟล์ .zip จากลิงก์ https://ecourse.cpe.ku.ac.th/download/cpeku_form.zip
2. เข้าสู่โปรแกรม KidBright เลือกเมนู Plugins &rarr; Install Plugins แล้วเลือกไฟล์ `cpeku_form.zip` ที่บันทึกเอาไว้


## การติดตั้ง วิธีที่ 2

1. ดาวน์โหลดไฟล์ .zip จากลิงก์ https://gitlab.com/cjaikaeo/cpeku_form/-/archive/master/cpeku_form.zip
   เมื่อแตกไฟล์ออกจะได้โฟลเดอร์ชื่อ `cpeku_form-master`
2. เปลี่ยนชื่อ `cpeku_form-master` ให้เป็น `cpeku_form` และนำไปวางไว้ในโฟลเดอร์ `plugins` ของ KidBright IDE


## บล็อกที่รองรับ

|  **บล็อก**  | **การทำงาน** |
| ---------- | ------------ |
| <img src="img/block-setlink.png"> | ตั้งค่าลิงก์ส่งข้อมูลล่วงหน้าที่สร้างขึ้นจาก Google Forms |
| <img src="img/block-submit.png"> | บันทึกฟอร์มด้วยค่าที่กำหนด |


## ตัวอย่างการใช้งาน

1. สร้างฟอร์มใหม่ใน [Google Forms](https://forms.google.com) แล้วป้อนคำถามชื่อ Temperature และ Light
   โดยกำหนดรูปแบบคำตอบเป็น Short answer
    <br><img src="img/form-design.png">

2. ตั้งค่าใน Settings เพื่ออนุญาตให้ส่งคำตอบได้โดยไม่ต้องมีการลงชื่อเข้าใช้งาน
   โดยเฉพาะอย่างยิ่งเมื่อสร้างฟอร์มผ่าน Google Workspace ของหน่วยงาน
    <br><img src="img/form-settings.png">

3. เลือกเมนู Get pre-filled link
    <br><img src="img/form-menu.png">

4. ป้อน `{0}` และ `{1}` ลงไปในคำตอบของ Temperature และ Light จากนั้นกดปุ่ม Get Link
    <br><img src="img/form-prefilled.png">

5. กดปุ่ม COPY LINK เพื่อคัดลอกลิงก์ที่ Google Forms สร้างขึ้นมาให้
    <br><img src="img/form-copylink.png">

6. ในโปรแกรม KidBright IDE สร้างบล็อกตามภาพ แล้วนำลิงก์ที่คัดลอกไว้มาวางในบล็อก `set pre-filled link to`
    <br><img src="img/example-en.png">

7. ตั้งค่าเครือข่ายไวไฟให้กับ KidBright และกดอัพโหลดโค้ด เมื่อเชื่อมต่อเครือข่ายไวไฟสำเร็จ
   (ไฟสถานะ WiFi สีแดงติดขึ้น) ทดลองกดปุ่ม S1 จะทำให้ KidBright
   ส่งค่าอุณหภูมิและแสงไปยังฟอร์มที่สร้างขึ้น

8. กลับมาที่หน้าการออกแบบฟอร์มและเลือกแท็บ Responses ในหน้านี้ควรมีค่าที่รับมาจากอุปกรณ์
   KidBright ปรากฏขึ้น กดปุ่ม Create Spreadsheet เพื่อดูค่าที่บันทึกไว้ผ่านแอปพลิเคชัน Google
   Sheets
    <br><img src="img/form-response.png">


## ข้อจำกัด

* รองรับการส่งข้อมูลในรูปตัวเลข (จำนวนเต็มหรือทศนิยม) เท่านั้น ยังไม่รองรับการส่งข้อมูลแบบข้อความ
* หาก Google มีการเปลี่ยนแปลงรูปแบบลิงก์การป้อนข้อมูลล่วงหน้าในอนาคต อาจทำให้บล็อกใช้งานไม่ได้


## ผู้พัฒนา

ผศ.ดร.ชัยพร ใจแก้ว ภาควิชาวิศวกรรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์

