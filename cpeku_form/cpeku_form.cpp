#include <functional>
#include <map>
#include <string>
#include <vector>
#include <cstdio>
#include <cstring>

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#include <esp_log.h>
#include <tcpip_adapter.h>

#include "esp_http_client.h"

#include "kidbright32.h"
#include "cpeku_form.h"

static const char *TAG = "CPEKU_FORM";

char CPEKU_FORM::_link[512];

static void compute_url(char *url, char *new_url, const std::vector<double>& data) {
    std::string s_url(url);
    char to_find[20], to_replace[20];
    int index;
    int len;

    // replace form-viewing action with response-submitting action
    strcpy(to_find, "/viewform?");
    strcpy(to_replace, "/formResponse?");
    index = s_url.find(to_find);
    if (index != std::string::npos) {
        s_url.replace(index, strlen(to_find), to_replace);
    }
    else {
        new_url[0] = 0;
        return;
    }

    // replace all the placeholders with actual data
    for (int i = 0; i < data.size(); i++) {
        len = std::sprintf(to_find, "%%7B%d%%7D", i);
        std::sprintf(to_replace, "%f", data[i]);
        while ((index=s_url.find(to_find)) != std::string::npos) {
            s_url.replace(index, len, to_replace);
        }
    }

    // 'submit' parameter also needs to be set
    s_url.append("&submit=Submit");

    // copy the string's content to the provided buffer
    strcpy(new_url, s_url.c_str());
}

static void extract_data(std::vector<double>& data, int count, va_list args) {
    // extract variadic arguments into a vector
    data.clear();
    for (int i = 0; i < count; i++) {
        data.push_back(va_arg(args, double));
    }
}

CPEKU_FORM::CPEKU_FORM() {
  _link[0] = 0;
}

void CPEKU_FORM::init(void) {
  esp_log_level_set(TAG, LOG_LOCAL_LEVEL);
  error = false;
  initialized = true;
}

void CPEKU_FORM::process(Driver *drv) {
}

void CPEKU_FORM::set_link(const char *link) {
    strncpy(_link, link, sizeof(_link));
    _link[sizeof(_link)-1] = 0;
    ESP_LOGI(TAG, "Set prefilled link to %s", _link);
}

void CPEKU_FORM::submit(int count, ...) {
    char url[512];
    std::vector<double> data;

    va_list args;
    va_start(args, count);
    extract_data(data, count, args);
    va_end(args);

    compute_url(_link, url, data);

    // make sure an IP address is obtained
    ESP_LOGI(TAG, "awaiting IP address");
    tcpip_adapter_ip_info_t ipInfo;
    while (1) {
      tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &ipInfo);
      if (ipInfo.ip.addr != 0)
        break;
      vTaskDelay(100 / portTICK_RATE_MS);
    }
    ESP_LOGI(TAG, "IP address detected");

    ESP_LOGI(TAG, "Submitting data to %s", url);
	esp_http_client_config_t config = {
        .url = url
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK) {
        ESP_LOGI(TAG, "HTTPS Status = %d, content_length = %d",
                esp_http_client_get_status_code(client),
                esp_http_client_get_content_length(client));
    } else {
        ESP_LOGE(TAG, "Error perform http request %s", esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);
}

