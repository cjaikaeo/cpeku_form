#ifndef __CPEKU_FORM_CLIENT_H__
#define __CPEKU_FORM_CLIENT_H__

#include <map>
#include "driver.h"
#include "device.h"
#include "esp_log.h"

class CPEKU_FORM : public Device {
  public:

    CPEKU_FORM();

    void init(void) override;
    void process(Driver *drv) override;
    int prop_count(void) override { return 0; }
    bool prop_name(int index, char *name) override { return false; }
    bool prop_unit(int index, char *unit) override { return false; }
    bool prop_attr(int index, char *attr) override { return false; }
    bool prop_read(int index, char *value) override { return false; }
    bool prop_write(int index, char *value) override { return false; }

    void set_link(const char* link);
    void submit(int count, ...);

  private:
    static char _link[512];
};

#endif

