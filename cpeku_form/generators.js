Blockly.JavaScript['cpeku_form_set_link'] = function(block) {
  var text_link = block.getFieldValue('link');
  var code = 'DEV_IO.CPEKU_FORM().set_link(' 
           + '"' + text_link + '");\n';
  return code;
};

Blockly.JavaScript['cpeku_form_submit'] = function(block) {
  var elements = new Array(block.itemCount_);
  for (var i = 0; i < block.itemCount_; i++) {
    elements[i] = Blockly.JavaScript.valueToCode(block, 'ADD' + i,
      Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  var code = 'DEV_IO.CPEKU_FORM().submit(' + elements.length + ', '
    + elements.map(x => '(double)' + (x != 'null' ? x : 0) ).join(',') + ');\n';
  return code;
};
