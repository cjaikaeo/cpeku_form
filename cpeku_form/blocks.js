Blockly.Blocks['cpeku_form_submit_container'] = {
  init: function() {
    this.appendDummyInput().appendField('Value List');
    this.appendStatementInput('STACK');
    this.contextMenu = false;
    this.setColour(195);
  },
};

Blockly.Blocks['cpeku_form_submit_item'] = {
  init: function() {
    this.appendDummyInput().appendField('Value');
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.contextMenu = false;
    this.setColour(195);
  },
};

Blockly.Blocks['cpeku_form_set_link'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_FORM_SET_LINK)
        .appendField(new Blockly.FieldTextInput("pre-filled form link"), "link");
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
  }
};

Blockly.Blocks['cpeku_form_submit'] = {
  init: function() {
    this.itemCount_ = 1;
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_FORM_SUBMIT);
    this.updateShape_();
    this.setOutput(false, 'Array');
    this.setMutator(new Blockly.Mutator(['cpeku_form_submit_item']));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(195);
  },

  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    // console.log('mutationToDom: ', container);
    return container;
  },

  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
    // console.log('domToMutation: ', this.itemCount_);
    this.updateShape_();
  },

  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('cpeku_form_submit_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 0; i < this.itemCount_; i++) {
      var itemBlock = workspace.newBlock('cpeku_form_submit_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    // console.log('decompose: ', containerBlock);
    return containerBlock;
  },

  compose: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    // Count number of inputs.
    var connections = [];
    while (itemBlock) {
      connections.push(itemBlock.valueConnection_);
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    // Disconnect any children that don't belong.
    for (var i = 0; i < this.itemCount_; i++) {
      var connection = this.getInput('ADD' + i).connection.targetConnection;
      if (connection && connections.indexOf(connection) == -1) {
        connection.disconnect();
      }
    }
    this.itemCount_ = connections.length;
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 0; i < this.itemCount_; i++) {
      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
    }
  },

  saveConnections: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 0;
    while (itemBlock) {
      var input = this.getInput('ADD' + i);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      i++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  },

  updateShape_: function() {
    // Add new inputs.
    for (var i = 0; i < this.itemCount_; i++) {
      if (!this.getInput('ADD' + i)) {
        var input = this.appendValueInput('ADD' + i).setCheck(['Number']);
        input.appendField(
          Blockly.Msg.CPEKU_FORM_SUBMIT_ITEM_MSG1
          + " {" + i + "} " 
          + Blockly.Msg.CPEKU_FORM_SUBMIT_ITEM_MSG2);
      }
    }
    // Remove deleted inputs.
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
  }
};
